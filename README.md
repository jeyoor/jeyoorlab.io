![Build Status](https://gitlab.com/pages/hakyll/badges/master/build.svg)

---

Personal home page written in hakyll

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Getting help](#getting-help)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

##Personal home page

Written with hakyll

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Hakyll.
1. Generate the website: `stack exec site build`
1. Preview your project: `stack exec site watch`
1. Add content

Read more at Hakyll's [documentation][hakyll].
